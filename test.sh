#!/bin/bash

BLOCKS=$((m4_DEVICE_SIZE / m4_BLOCK_SIZE))

MODULE=$(sed -rn 's/.*WLFS_NAME "(.*?)".*/\1/p' include/wlfs.h)
# Reload the kernel module if necessary
if [ -n "$(lsmod | grep $MODULE)" ]
then
    sudo rmmod $MODULE
fi
sudo insmod ${MODULE}.ko

# Zero and reformat the test device
dd if=/dev/zero of=m4_DEVICE bs=m4_BLOCK_SIZE count=$BLOCKS
./mkfs-wlfs -b m4_BLOCK_SIZE -n $((1 << 20)) m4_DEVICE

# Create the mount point if it doesn't exist
if [ ! -d m4_MOUNT ]
then
    mkdir m4_MOUNT
fi

# Run unit tests
sudo ./test/unit_test
# Run validation tests
if ! $(sudo mount -o loop -t $MODULE m4_DEVICE m4_MOUNT)
then
    echo "Mounting unsuccessful"
    exit 1
fi
./test/validation_test

# Clean up
sudo umount m4_MOUNT
sudo rmmod $MODULE
