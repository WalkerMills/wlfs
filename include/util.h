/*
 * Utility functions for wlfs
 */

#pragma once

#include "wlfs.h"

#ifdef __KERNEL__
#include <linux/printk.h>
#define ENTER_FN printk(KERN_INFO "Entering %s\n", __FUNCTION__);
#endif

#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))

#ifdef __cplusplus
extern "C" {
#endif

/*
 *  General purpose utilities
 */

// Number of bytes of data in each block (not including header)
int block_bytes (const struct wlfs_super_meta *meta);

// Check if the highest set bit in <value> is beyond the lowest <bits> bits;
// returns true iff casting <value> into a type <bits> wide will overflow
int will_overflow (__u64 value, __u8 bits);

// First block number of the checkpoint region
sector_t checkpoint_start (const struct wlfs_super_meta *meta);

// Maximum number of bytes in a file
__u64 max_bytes (const struct wlfs_super_meta *meta);

// First block number of the given log segment
sector_t segment_start (const struct wlfs_super_meta *meta, __u32 segment);

/*
 *  Imap block helpers
 */

// Segmap block containing the given inode
__u32 imap_block (const struct wlfs_super_meta *meta, __u32 inode);

// Number of inode map blocks
__u32 imap_blocks (const struct wlfs_super_meta *meta);

// Number of entries (block numbers) per imap block
int imap_entries (const struct wlfs_super_meta *meta);

// Index of an inode within an imap block
int imap_entry(const struct wlfs_super_meta *meta, __u32 inode);

/*
 *  Segmap block helpers
 */

// Segmap block containing the given segment
__u32 segmap_block (const struct wlfs_super_meta *meta, __u32 segment);

// Number of segmap blocks
__u32 segmap_blocks (const struct wlfs_super_meta *meta);

// Number of entries (blocks written in a segment) per segmap block
int segmap_entries (const struct wlfs_super_meta *meta);

// Index of a segment within a segmap block
int segmap_entry(const struct wlfs_super_meta *meta, __u32 segment);

/*
 *  Checkpoint block helpers
 */

// Checkpoint block containing the given index
__u32 checkpoint_block (const struct wlfs_super_meta *meta, __u32 index);

// Number of checkpoint blocks
__u32 checkpoint_blocks (const struct wlfs_super_meta *meta);

// Number of entries (block numbers) per checkpoint block
int checkpoint_entries (const struct wlfs_super_meta *meta);

// Index of a checkpoint entry within a block
int checkpoint_entry (const struct wlfs_super_meta *meta, __u32 index);

// Specify whether an index is in the segmap or imap
#define SEGMAP_M 0
#define IMAP_M 1
#define MAP_MASK (SEGMAP_M | IMAP_M)
// Specify whether an index refers to a block or an entry
#define BLOCK_M (1 << 1)
#define ENTRY_M (1 << 2)
#define INDEX_MASK (BLOCK_M | ENTRY_M)

// Convert an entry index in the segmap or imap to a block index in the
// checkpoint region
__u32 checkpoint_index (const struct wlfs_super_meta *meta, __u32 index, 
                        unsigned type);

// Block number of the checkpoint block containing the given index
sector_t checkpoint_offset (const struct wlfs_super_meta *meta, __u32 index,
                            unsigned type);

#ifdef __cplusplus
};
#endif
