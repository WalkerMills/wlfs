#pragma once

#include <linux/types.h>
#ifndef __KERNEL__
#include <stddef.h>

// Imap & checkpoint entries are block numbers, but for whatever reason the 
// sector_t from the C stdlib is 4 bytes, not 8 like the Linux type
typedef __u64 sector_t;
#endif


struct header {
    __kernel_time_t wtime;
    // Incremented when the file is deleted/trunctated
    __u8 version;
};

// This is basically a block header; there's always one at the head of a
// block, and the remaining space is data
struct block {
    // Two headers, in case of mid-update crashes
    struct header h0;
    struct header h1;
    // Could be inode #, or map block #
    __u64 index;
};

// Segmap entry type
typedef __u16 __segmap_t;

#ifdef __cplusplus
extern "C" {
#endif

void *block_entry_ptr (struct block *block, int entry, size_t size);

#ifdef __cplusplus
};
#endif

#define block_entry(block, entry, type) ((type *) block_entry_ptr( \
block, entry, sizeof(type)))
