#pragma once

#include <asm/atomic.h>
#include <linux/list.h>
#include <linux/radix-tree.h>
#include <linux/slab.h>
#include <linux/spinlock.h>
#include <linux/types.h>

#include "block.h"
#include "super.h"
#include "util.h"

struct wlfs_super;
struct block;

struct block_queue {
    struct block* data;
    struct list_head list;
    // Lock for the block in this queue entry
    seqlock_t lock;
    // Ref count for the block in this queue entry 
    atomic_t refs;
    // Indicates whether this block has been modified while in the cache
    atomic_t dirty;
};

// Constants for indicated a block is clean or dirty
#define __CLEAN 0
#define __DIRTY 1

// A LRU cache for blocks
struct block_cache {
    // Cache for data blocks
    struct kmem_cache *block_cache;
    // Cache for queue entries
    struct kmem_cache *queue_cache;

    // Radix tree mapping indexes of cached blocks to pointers (in a queue)
    struct radix_tree_root live;
    // Queue of active cached blocks (ref count > 0)
    struct block_queue *active;
    // Queue of inactive cached blocks (ref count == 0)
    struct block_queue *inactive;

    // Lock for live block tree
    rwlock_t live_lock;
    // Locks for the block queues. These are for the overall data structures;
    // blocks in the queues have their own locks. For example, one must get a
    // read lock on a queue and then a write lock on a block within it in
    // order to modify that block's contents (but not the queue itself)
    rwlock_t active_lock;
    rwlock_t inactive_lock;

    // Number of cached blocks
    atomic_t cached;
    // Upper bound on number of cached blocks
    atomic_t limit;

    // The following are assumed to be constant for the life of the cache

    // Super block of host filesystem
    struct super_block *sb;
    // Function for persisting dirty blocks when they are evicted
    void (*persist) (struct super_block *, struct block *);
    // Block size (bytes)
    size_t block_size;
    // Entry size (bytes)
    size_t entry_size;
    // Number of entries per block (used to map entry indexes to block indexes)
    unsigned entries;
    // Mask for allocating cache entries
    gfp_t mask;
};

// Allocate a new block cache; will return an error pointer on failure
struct block_cache *alloc_block_cache (
    struct super_block *sb,
    char *name, 
    long limit, 
    size_t block_size,
    size_t entry_size,
    gfp_t mask,
    void (*persist) (struct super_block *, struct block *));

// Remove all blocks from the cache
void shrink_block_cache (struct block_cache *cache);

// Free the given block cache
void free_block_cache (struct block_cache *cache);

// Access a block in the cache by index; the index may refer to either a
// block, or a block entry.  If it is a block entry, this function returns
// the containing block.  Returns NULL if the block is not in the cache
struct block *access_block (struct block_cache *cache, __u32 index,
                            unsigned type);

// Read a block into the cache; if the block is already in the cache, it will
// not be replaced, but this will incur a read as long as the cache isn't full
struct block *cache_block (struct block_cache *cache, sector_t offset);

// Get a block from the cache, or read it from disk if necessary.
struct block *get_block (struct block_cache *cache, sector_t offset, 
                         __u32 index, unsigned type);

// Release a reference to a cached block
int release_block (struct block_cache *cache, struct block *block);

// Set the block cache's limit (maximum cache occupancy)
void set_limit (struct block_cache *cache, long limit);

// Copy the entry_size bytes at the specified index to the given memory
// location.  This will read the block if it is not in the cache already
int get_entry (struct block_cache *cache, sector_t offset, __u32 index,
               void *dest); 
// Same as above, but the block must already be cached
int get_entry_cached (struct block_cache *cache, struct block *block,
                      __u32 index, void *dest);

// Copy the entry_size bytes from the given memory address to the specified
// index in the appropriate block.  This will read the block if it is not
// in the cache already
int set_entry (struct block_cache *cache, sector_t offset, __u32 index,
               void *src);
// Same as above, but the block must already be cached
int set_entry_cached (struct block_cache *cache, struct block *block,
                      __u32 index, void *src);

