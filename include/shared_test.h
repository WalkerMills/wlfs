#pragma once

#include <linux/types.h>

#include "block.h"
#include "wlfs.h"

#define SEP ":"
#define MSG_TYPE 0xF00
#define BUF_SIZE 128

enum test_func {
    TEST_MAX_BYTES,
    TEST_CHECKPOINT_BLOCK_CACHE,
};

struct test_max_bytes_args {
    struct wlfs_super_meta meta;
};

struct test_checkpoint_block_cache_args {
    int block_size;
    int entry_size;
#define __TEST_CHECKPOINT_BLOCK_CACHE_BLOCKS 2
    __u64 index[__TEST_CHECKPOINT_BLOCK_CACHE_BLOCKS];
    __u64 offset[__TEST_CHECKPOINT_BLOCK_CACHE_BLOCKS];
    __u32 entry_index;
    __u64 entry;
};
