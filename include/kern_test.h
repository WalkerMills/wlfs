#pragma once

#ifndef NDEBUG
#include <linux/skbuff.h>

int test_max_bytes (struct sk_buff *, void __user *);

int test_checkpoint_block_cache (struct sk_buff *, void __user *);
#endif