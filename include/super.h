/*
 * Super block data structure & methods
 */

#pragma once

#include <linux/fs.h>

#include "cache.h"
#include "wlfs.h"

// Intialize the superblock
struct dentry *wlfs_mount (struct file_system_type *type, int flags,
                           char const *dev, void *data);
// Free the superblock
void wlfs_kill_sb (struct super_block *sb);

struct wlfs_super {
    struct super_block *sb;
    struct wlfs_super_meta meta;
    struct block_cache *checkmap;
    struct block_cache *segmap;
    struct block_cache *imap;
};
