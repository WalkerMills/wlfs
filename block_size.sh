#!/bin/bash

if [ -b m4_DEVICE ]
then
   echo -n $(lsblk -o NAME,PHY-SEC | grep $(basename m4_DEVICE) | awk '{print $2}')
else
   echo -n $(lsblk -o MOUNTPOINT,PHY-SEC | grep "$(df $(dirname m4_DEVICE) --output=target | tail -n 1) " | awk '{print $2}')
fi

