#include <asm/bug.h>
#include <linux/buffer_head.h>
#include <linux/compiler.h>
#include <linux/dcache.h>
#include <linux/err.h>
#include <linux/errno.h>
#include <linux/init.h>
#include <linux/list.h>
#include <linux/printk.h>
#include <linux/string.h>
#include <linux/time.h>

#include "super.h"
#include "util.h"

// Deallocate imap, segmap, and object caches within the superblock
static void wlfs_put_super (struct super_block *sb);
// Populate the in-memory superblock with data from disk & computed fields
static int wlfs_fill_super (struct super_block *sb, void *data, int silent);

static struct super_operations const wlfs_super_ops = {
    .put_super = wlfs_put_super,
};

struct dentry *wlfs_mount (struct file_system_type *type, int flags,
                           char const *dev, void *data) {
#ifndef NDEBUG
    printk(KERN_DEBUG "Mounting wlfs\n");
#endif
    struct dentry *const entry = mount_bdev(type, flags, dev, data, 
                                            wlfs_fill_super);
    if (unlikely(IS_ERR(entry))) {
        printk(KERN_ERR "Failed to mount wlfs\n");
    } else {
        printk(KERN_INFO "Successfully mounted wlfs\n");
    }

    return entry;
}

void wlfs_kill_sb (struct super_block *sb) {
#ifndef NDEBUG
    printk(KERN_DEBUG "Freeing superblock\n");
#endif
    kill_block_super(sb);
    printk(KERN_INFO "Freed superblock\n");
}

void wlfs_put_super (struct super_block *sb) {
#ifndef NDEBUG
    printk(KERN_DEBUG "Destroying superblock members\n");
#endif
    struct wlfs_super *wlfs_sb = (struct wlfs_super *) sb->s_fs_info;
    free_block_cache(wlfs_sb->checkmap);
    free_block_cache(wlfs_sb->segmap);
    free_block_cache(wlfs_sb->imap);
    kfree(wlfs_sb);
}

int wlfs_fill_super (struct super_block *sb, void *data, int silent) {
#ifndef NDEBUG
    printk(KERN_DEBUG "Populating super block fields\n");
#endif
    int ret = -EPERM;

    // Read the on-disk superblock into memory
    struct buffer_head *bh = sb_bread(sb, 
                                      WLFS_OFFSET / sb->s_bdev->bd_block_size);
    BUG_ON(!bh);
    struct wlfs_super *wlfs_sb = 
        (struct wlfs_super *) kzalloc(sizeof(struct wlfs_super), GFP_NOFS);
    BUG_ON(!wlfs_sb);
    memcpy(&wlfs_sb->meta, bh->b_data, sizeof(struct wlfs_super_meta));
    brelse(bh);
    // Check that the data was read & copied correctly
    BUG_ON(wlfs_sb->meta.magic != WLFS_MAGIC);

    // Set superblock fields
    wlfs_sb->sb = sb;
    sb->s_magic = wlfs_sb->meta.magic;
    sb->s_op = &wlfs_super_ops;
    sb->s_fs_info = wlfs_sb;
    if (unlikely(sb_set_blocksize(sb, wlfs_sb->meta.block_size)) == 0) {
        printk(KERN_ERR "Error setting block size to %u\n", 
               wlfs_sb->meta.block_size);
        ret = -EINVAL;
        goto exit;
    }
    sb->s_maxbytes = max_bytes(&wlfs_sb->meta);

    // Allocate caches
    wlfs_sb->checkmap = alloc_block_cache(
        sb, WLFS_NAME "_checkmap", MAX_OCCUPANCY, wlfs_sb->meta.block_size,
        sizeof(sector_t), GFP_NOFS, NULL);
    wlfs_sb->segmap = alloc_block_cache(
        sb, WLFS_NAME "_segmap", MAX_OCCUPANCY, wlfs_sb->meta.block_size, 
        sizeof(__segmap_t), GFP_NOFS, NULL);
    wlfs_sb->imap = alloc_block_cache(
        sb, WLFS_NAME "_imap", MAX_OCCUPANCY, wlfs_sb->meta.block_size, 
        sizeof(sector_t), GFP_NOFS, NULL);
    
    // Cache checkpoint block pointing to the current segmap block
    __u32 index = checkpoint_index(
        &wlfs_sb->meta, wlfs_sb->meta.current_segment, SEGMAP_M);
    struct block *block = cache_block(
        wlfs_sb->checkmap, 
        checkpoint_offset(&wlfs_sb->meta, index, ENTRY_M));
    // Cache the current segmap block
    sector_t offset = *block_entry(
        block, checkpoint_entry(&wlfs_sb->meta, index), sector_t);
    release_block(wlfs_sb->checkmap, block);
    block = cache_block(wlfs_sb->segmap, offset);
    BUG_ON(block->index != 
           segmap_block(&wlfs_sb->meta, wlfs_sb->meta.current_segment));
    release_block(wlfs_sb->segmap, block);
    
    // Cache the checkpoint block pointing to the first imap block
    index = checkpoint_index(&wlfs_sb->meta, ROOT_INODE_INDEX, IMAP_M);
    block = get_block(
        wlfs_sb->checkmap, 
        checkpoint_offset(&wlfs_sb->meta, index, ENTRY_M),
        index,
        ENTRY_M);
    // Cache the first imap block
    offset = *block_entry(
        block, checkpoint_entry(&wlfs_sb->meta, index), sector_t);
    release_block(wlfs_sb->checkmap, block);
    block = cache_block(wlfs_sb->imap, offset);
    BUG_ON(block->index != 0);
    release_block(wlfs_sb->imap, block);

    // Intialize root inode
    struct inode *root = new_inode(sb);
    root->i_ino = ROOT_INODE_INDEX;
    inode_init_owner(root, NULL, S_IFDIR);
    root->i_sb = sb;
    root->i_ctime = root->i_atime = root->i_mtime = CURRENT_TIME;
    sb->s_root = d_make_root(root);
    if (unlikely(!sb->s_root)) {
        printk(KERN_ERR "Error allocating root inode\n");
        ret = -ENOMEM;
        goto exit;
    }

    ret = 0;
exit:
    return ret;
}
