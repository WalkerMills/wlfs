/*
 * Module initialization & cleanup handlers
 */ 
#include <linux/compiler.h>
#include <linux/module.h>
#include <linux/printk.h>

#include "wlfs.h"
#include "super.h"

#ifndef NDEBUG
#include <linux/string.h>
#include <net/netlink.h>
#include <net/sock.h>

#include "kern_test.h"
#include "shared_test.h"

static int (*test_ops[]) (struct sk_buff *, void *) = {
    &test_max_bytes,
    &test_checkpoint_block_cache,
};

static void forward_test_op (struct sk_buff *skb) {
    struct nlmsghdr *header = (struct nlmsghdr *) skb->data;
    BUG_ON(header->nlmsg_type != MSG_TYPE);
    char *msg = NLMSG_DATA(header);
    BUG_ON(strlen(msg) > BUF_SIZE);
    char buf[BUF_SIZE] = {0};
    int index;
    __u64 src;

    // Pointer to first separator character
    char *sep = strpbrk(msg, SEP);
    // Extract the test index (in test_ops)
    strncpy(buf, msg, sep - msg);
    BUG_ON(kstrtoint(buf, 10, &index) < 0);
    BUG_ON(sizeof(test_ops) / sizeof(test_ops[0]) <= index);
    // Move the string pointer past the separator(s)
    msg = sep + strspn(sep, SEP);
    // Extract the pointer to an argument struct
    strncpy(buf, msg, strlen(msg));
    BUG_ON(kstrtou64(buf, 16, &src) < 0);

    // Run the indicated test
    if ((*test_ops[index])(skb, (void *) src) < 0) {
        printk(KERN_ERR "Test failed\n");
    }
}

static struct sock *socket;
#endif

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Walker Mills");
MODULE_DESCRIPTION("Walker's log-based filesystem");

static struct file_system_type wlfs_type = {
    .owner = THIS_MODULE,
    .name = WLFS_NAME,
    .mount = wlfs_mount,
    .kill_sb = wlfs_kill_sb,
    .fs_flags = FS_REQUIRES_DEV,
};

static int __init wlfs_init (void) {
#ifndef NDEBUG
    printk(KERN_DEBUG "Loading wlfs\n");
#endif
    if (unlikely(register_filesystem(&wlfs_type))) {
        printk(KERN_ERR "Failed to register filesystem\n");
        return -1;
    }

#ifndef NDEBUG
    struct netlink_kernel_cfg cfg = {
        .input = &forward_test_op,
    };
    socket = netlink_kernel_create(&init_net, NETLINK_USERSOCK, &cfg);
    if (unlikely(!socket)) {
        printk(KERN_ERR "Failed to create netlink socket\n");
        return -1;
    }
#endif

    printk(KERN_INFO "Loaded wlfs\n");
    return 0;
}

static void __exit wlfs_exit (void) {
#ifndef NDEBUG
    printk(KERN_DEBUG "Unloading wlfs\n");
    netlink_kernel_release(socket);
#endif
    if (unlikely(unregister_filesystem(&wlfs_type))) {
        printk(KERN_ERR "Failed to unregister filesystem\n");
    } else {
        printk(KERN_INFO "Unloaded wlfs\n");
    }
}

module_init(wlfs_init);
module_exit(wlfs_exit);
