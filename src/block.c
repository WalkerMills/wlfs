#include "block.h"

void *block_entry_ptr (struct block *block, int entry, size_t size) {
    return (void *) (((__u8 *) (block + 1)) + entry * size);
}
