#include <linux/buffer_head.h>
#include <linux/err.h>
#include <linux/printk.h>
#include <linux/string.h>

#include "cache.h"
#include "util.h"

static void __decrement_block_refs (struct block_cache *cache,
                                    struct block_queue *entry);
static void __increment_block_refs (struct block_cache *cache, 
                                    struct block_queue *entry);
#define __INACTIVE 0
#define __ACTIVE 1
static int __pop_block (struct block_cache *cache, unsigned active);
static struct block *__push_new_block (struct block_cache *cache, 
                                       struct block *block);

struct block_cache *alloc_block_cache (
    struct super_block *sb,
    char *name,
    long limit, 
    size_t block_size, 
    size_t entry_size, 
    gfp_t mask,
    void (*persist) (struct super_block *, struct block *)) {
    // Validate cache parameters
#define NAME_LEN 128
    if (strlen(name) >= NAME_LEN - 13) {
        printk(KERN_ERR "Cache name must be < %d\n", NAME_LEN - 13);
        return ERR_PTR(-ENAMETOOLONG);
    }
    if (limit == 0) {
        printk(KERN_ERR "Cache size must be greater than 0\n");
        return ERR_PTR(-EINVAL);
    }
    unsigned entries = (block_size - sizeof(struct block)) / entry_size;
    if (entries == 0) {
        printk(KERN_ERR "At least one entry must fit into %lu bytes\n", 
               block_size - sizeof(struct block));
        return ERR_PTR(-EINVAL);
    }

    // Allocate the cache itself
    struct block_cache *cache = (struct block_cache *) kzalloc(
        sizeof(struct block_cache), mask);
    if (!cache) {
        printk(KERN_ERR "Failed to allocate memory for block_cache struct\n");
        return ERR_PTR(-ENOMEM);
    }
    // Allocate block & queue caches
    char buf[NAME_LEN];
    snprintf(buf, NAME_LEN, "%s_block_cache", name);
    cache->block_cache = kmem_cache_create(buf, block_size, 0, 0, NULL);
    if (!cache->block_cache) {
        printk(KERN_ERR "Failed to create block cache\n");
        kfree(cache);
        return ERR_PTR(-ENOMEM);
    }
    snprintf(buf, NAME_LEN, "%s_queue_cache", name);
    cache->queue_cache = kmem_cache_create(buf, sizeof(struct block_queue), 0, 
                                           0, NULL);
    if (!cache->queue_cache) {
        printk(KERN_ERR "Failed to create queue cache\n");
        kmem_cache_destroy(cache->block_cache);
        kfree(cache);
        return ERR_PTR(-ENOMEM);
    }
    // Initialize cache data structures
    INIT_RADIX_TREE(&cache->live, mask);
    cache->active = NULL;
    cache->inactive = NULL;
    rwlock_init(&cache->active_lock);
    rwlock_init(&cache->inactive_lock);
    rwlock_init(&cache->live_lock);

    // Populate cache struct with remaining input parameters
    atomic_set(&cache->limit, limit);
    atomic_set(&cache->cached, 0);
    cache->sb = sb;
    cache->persist = persist;
    cache->block_size = block_size;
    cache->entry_size = entry_size;
    cache->entries = entries;
    cache->mask = mask;

    return cache;
}

void shrink_block_cache (struct block_cache *cache) {
    // Evict all blocks from the cache
    while (cache->inactive != NULL) {
        __pop_block(cache, __INACTIVE);
    }
}

void free_block_cache (struct block_cache *cache) {
    // Empty the cache
    shrink_block_cache(cache);
    while (cache->active != NULL) {
        __pop_block(cache, __ACTIVE);
    }
    // Free the block & queue caches
    kmem_cache_destroy(cache->block_cache);
    kmem_cache_destroy(cache->queue_cache);
    // Free the cache itself
    kfree(cache);
}

struct block *access_block (struct block_cache *cache, __u32 index, 
                            unsigned type) {
    if (atomic_read(&cache->cached) == 0) {
        return NULL;
    }
    // If the index is for an entry, convert it to a block index
    if ((type & INDEX_MASK) == ENTRY_M) {
        index /= cache->entries;
    }
    // Check if the block is in the cache
    read_lock(&cache->live_lock);
    struct block_queue *queue_entry = radix_tree_lookup(&cache->live, index);
    // Return NULL if it isn't
    if (queue_entry == NULL) {
        read_unlock(&cache->live_lock);
        return NULL;
    }
    __increment_block_refs(cache, queue_entry);
    read_unlock(&cache->live_lock);
    return queue_entry->data;
}

struct block *cache_block (struct block_cache *cache, sector_t offset) {
    // If the cache is full, evict a block
    if (atomic_read(&cache->cached) == atomic_read(&cache->limit) && 
        __pop_block(cache, __INACTIVE) != 0) {
        return ERR_PTR(-ENOMEM);
    }
    // Read the block from the block device
    struct buffer_head *bh = sb_bread(cache->sb, offset);
    BUG_ON(!bh);
    // Add the block to the cache
    struct block *block = 
        __push_new_block(cache, (struct block *) bh->b_data);
    brelse(bh);

    return block;
}

struct block *get_block (struct block_cache *cache, sector_t offset, 
                         __u32 index, unsigned type) {
    // If the index is for an entry, convert it to a block index
    if ((type & INDEX_MASK) == ENTRY_M) {
        index /= cache->entries;
    }

    // Try and get the block from the cache
    struct block *block = access_block(cache, index, BLOCK_M);
    // Read it from the device if it's not in the cache
    if (block == NULL) {
        block = cache_block(cache, offset);
    }

    return block;
}

int release_block (struct block_cache *cache, struct block *block) {
    if (cache == NULL || block == NULL) {
        return -EINVAL;
    }
    read_lock(&cache->live_lock);
    struct block_queue *entry = radix_tree_lookup(&cache->live, block->index);
    __decrement_block_refs(cache, entry);
    read_unlock(&cache->live_lock);

    return 0;
}

inline void set_limit (struct block_cache *cache, long limit) {
    atomic_set(&cache->limit, limit);
}

int get_entry (struct block_cache *cache, sector_t offset, __u32 index, 
               void *dest) {
    struct block *block = get_block(cache, offset, index, ENTRY_M);
    int ret = get_entry_cached(cache, block, index, dest);
    release_block(cache, block);

    return ret;
}

int get_entry_cached (struct block_cache *cache, struct block *block,
                      __u32 index, void *dest) {
    // Make sure the index is actually in this block
    if (block->index != (index / cache->entries)) {
        printk(KERN_ERR "Entry %u is in block %u, not %llu\n", index, 
               index / cache->entries, block->index);
        return -EINVAL;
    }

    read_lock(&cache->live_lock);
    struct block_queue *entry = radix_tree_lookup(&cache->live, block->index);
    read_lock(&cache->active_lock);
    read_unlock(&cache->live_lock);

    if (entry == NULL) {
        read_unlock(&cache->active_lock);
        printk(KERN_ERR "Block %llu not cached\n", block->index);
        return -EFAULT;
    }

    void *src = 
        block_entry_ptr(block, index % cache->entries, cache->entry_size);
    unsigned seq;
    do {
        seq = read_seqbegin(&entry->lock);
        memcpy(dest, src, cache->entry_size);
    } while (read_seqretry(&entry->lock, seq));

    read_unlock(&cache->active_lock);
    return 0;
}

int set_entry (struct block_cache *cache, sector_t offset, __u32 index, 
               void *src) {
    struct block *block = get_block(cache, offset, index, ENTRY_M);
    int ret = set_entry_cached(cache, block, index, src);
    release_block(cache, block);

    return ret;
}

int set_entry_cached (struct block_cache *cache, struct block *block,
                      __u32 index, void *src) {
    // Make sure the index is actually in this block
    if (block->index != (index / cache->entries)) {
        printk(KERN_ERR "Entry %u is in block %u, not %llu\n", index, 
               index / cache->entries, block->index);
        return -EINVAL;
    }

    read_lock(&cache->live_lock);
    struct block_queue *entry = radix_tree_lookup(&cache->live, block->index);
    read_lock(&cache->active_lock);
    read_unlock(&cache->live_lock);

    if (entry == NULL) {
        read_unlock(&cache->active_lock);
        printk(KERN_ERR "Block %llu not cached\n", block->index);
        return -EFAULT;
    }
    // Address of the specified entry in the block
    void *dest = 
        block_entry_ptr(block, index % cache->entries, cache->entry_size);
    // Set the value of the entry
    write_seqlock(&entry->lock);
    memcpy(dest, src, cache->entry_size);
    write_sequnlock(&entry->lock);
    atomic_set(&entry->dirty, __DIRTY);

    read_unlock(&cache->active_lock);
    return 0;
}

void __decrement_block_refs (struct block_cache *cache,
                             struct block_queue *entry) {
    // If there is more than one reference, this block will stay in the active
    // queue
    if (atomic_read(&entry->refs) > 1) {
        atomic_dec(&entry->refs);
        return;
    }

    write_lock(&cache->inactive_lock);
    write_lock(&cache->active_lock);
    // Remove the containing entry from the active queue
    if (list_empty(&entry->list)) {
        cache->active = NULL;
    } else {
        list_del(&entry->list);
    }
    write_unlock(&cache->active_lock);
    // Set the ref count to zero
    atomic_set(&entry->refs, 0);
    // Push it onto the inactive queue
    if (cache->inactive == NULL) {
        INIT_LIST_HEAD(&entry->list);
        cache->inactive = entry;
    } else {
        list_add_tail(&entry->list, &cache->inactive->list);
    }

    write_unlock(&cache->inactive_lock);
}

void __increment_block_refs (struct block_cache *cache,
                             struct block_queue *entry) {
    // If there is at least one reference, this block is already in the active
    // queue
    if (atomic_read(&entry->refs) > 0) {
        atomic_inc(&entry->refs);
        return;
    }

    write_lock(&cache->inactive_lock);
    write_lock(&cache->active_lock);
    // Remove the containing entry from the inactive queue
    if (list_empty(&entry->list)) {
        cache->inactive = NULL;
    } else {
        list_del(&entry->list);
    }
    write_unlock(&cache->inactive_lock);
    // Set the ref count to one
    atomic_set(&entry->refs, 1);
    // Push it onto the active queue
    if (cache->active == NULL) {
        INIT_LIST_HEAD(&entry->list);
        cache->active = entry;
    } else {
        list_add_tail(&entry->list, &cache->active->list); 
    }

    write_unlock(&cache->active_lock);
}

int __pop_block (struct block_cache *cache, unsigned active) {
    struct block_queue **queue = active ? &cache->active : &cache->inactive;
    rwlock_t *queue_lock = 
        active ? &cache->active_lock : &cache->inactive_lock;
    write_lock(&cache->live_lock);
    write_lock(queue_lock);
    if (*queue == NULL) {
        write_unlock(queue_lock);
        write_unlock(&cache->live_lock);
        return -EINVAL;
    }
    // Remove the leaf for the queue head from the index radix tree
    radix_tree_delete(&cache->live, (*queue)->data->index);
    write_unlock(&cache->live_lock);
    // Store the old queue head
    struct block_queue *entry = *queue;

    // If there is only one cached block
    if (list_empty(&(*queue)->list) || (*queue)->list.next == LIST_POISON2) {
        // Reset the queue
        *queue = NULL;
    } else {
        // Pop the queue
        struct block_queue *new_head = list_next_entry(entry, list);
        list_del(&entry->list);
        // Replace the head in the cache
        *queue = new_head;
    }
    write_unlock(queue_lock);
    atomic_dec(&cache->cached);

    // Persist the block if it is dirty
    if (cache->persist != NULL && atomic_read(&entry->dirty) == __DIRTY) {
        cache->persist(cache->sb, entry->data);
    }
    // Evict the block & queue entry from their caches
    kmem_cache_free(cache->block_cache, entry->data);
    kmem_cache_free(cache->queue_cache, entry);

    return 0;
}

struct block *__push_new_block (struct block_cache *cache, 
                                struct block *block) {
    if (atomic_read(&cache->cached) == atomic_read(&cache->limit)) {
        printk(KERN_ERR "Block cache full\n");
        return ERR_PTR(-ENOMEM);
    }
    
    // Create a new entry for the block queue
    struct block_queue *entry = kmem_cache_alloc(cache->queue_cache,
                                                 cache->mask);
    // Copy the block into the cache
    entry->data = kmem_cache_alloc(cache->block_cache, cache->mask);
    seqlock_init(&entry->lock);
    atomic_set(&entry->refs, 1);
    atomic_set(&entry->dirty, __CLEAN);
    memcpy(entry->data, block, cache->block_size);

    // Add a leaf to the radix tree tracking active blocks
    write_lock(&cache->live_lock);
    write_lock(&cache->active_lock);
    int ret = radix_tree_insert(
        &cache->live, entry->data->index, entry);
    write_unlock(&cache->live_lock);
    if (ret < 0) {
        printk(KERN_ERR "Inserting pointer into radix tree failed: %i\n", ret);
        kmem_cache_free(cache->block_cache, entry->data);
        kmem_cache_free(cache->queue_cache, entry);
        write_unlock(&cache->active_lock);
        return ERR_PTR(ret);
    }
    // Push the block onto the queue itself
    if (cache->active == NULL) {
        INIT_LIST_HEAD(&entry->list);
        cache->active = entry;
    } else {
        list_add_tail(&entry->list, &cache->active->list);
    }
    write_unlock(&cache->active_lock);

    atomic_inc(&cache->cached);
    return entry->data;
}
