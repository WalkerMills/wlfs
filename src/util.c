#ifdef __KERNEL__
#include <asm-generic/bug.h>
#endif

#include "block.h"
#include "util.h"

/*
 *  General purpose utilities
 */

int block_bytes (const struct wlfs_super_meta *meta) {
    return meta->block_size - sizeof(struct block);
}

int will_overflow (__u64 value, __u8 bits) {
    __u64 mask;
    if (bits == (sizeof(unsigned long long) << 3)) {
        mask = ~0;
    } else {
        mask = (1ULL << bits) - 1;
    }
    return (value & mask) != value;
}

sector_t checkpoint_start (const struct wlfs_super_meta *meta) {
    return WLFS_OFFSET / meta->block_size + 1;
}

// Max file size = block size * (
//      local block pointers per inode - 
//      indirection +
//      sum_{i=0}^{indirection} (entries per indirect block)^i)
__u64 max_bytes (const struct wlfs_super_meta *meta) {
    __u64 size = NBLOCK_PTR;

    if (meta->indirection < 1) {
        goto exit;
    }

    __u64 block_entries = block_bytes(meta) / sizeof(sector_t);
    size = size - meta->indirection + block_entries;
    int i;
    for (i = 2; i <= meta->indirection; ++i) {
        __u64 base = block_entries;
        int j;
        for (j = i - 1; j > 0; --j) {
            base *= block_entries;
        }
        size += base;
    }

exit:
    size *= meta->block_size;
    return size;
}

sector_t segment_start (const struct wlfs_super_meta *meta, __u32 segment) {
    return checkpoint_start(meta) + meta->checkpoint_blocks +
           segment * (meta->segment_size / meta->block_size);
}

static inline __u32 ceil32 (__u32 dividend, __u32 divisor) {
    return dividend / divisor + (dividend % divisor != 0);
}

/*
 *  Imap block helpers
 */

__u32 imap_block (const struct wlfs_super_meta *meta, __u32 inode) {
    return (inode - ROOT_INODE_INDEX) / imap_entries(meta);
}

__u32 imap_blocks (const struct wlfs_super_meta *meta) {
    return MAX(ceil32(meta->inodes, imap_entries(meta)), 1);
}

int imap_entries (const struct wlfs_super_meta *meta) {
    return block_bytes(meta) / sizeof(sector_t);
}

int imap_entry (const struct wlfs_super_meta *meta, __u32 inode) {
    return (inode - ROOT_INODE_INDEX) % imap_entries(meta);
}

/*
 *  Segmap block helpers
 */

__u32 segmap_block (const struct wlfs_super_meta *meta, __u32 segment) {
    return segment / segmap_entries(meta);
}

__u32 segmap_blocks (const struct wlfs_super_meta *meta) {
    return MAX(ceil32(meta->segments, segmap_entries(meta)), 1);
}

int segmap_entries (const struct wlfs_super_meta *meta) {
    return block_bytes(meta) / sizeof(__segmap_t);
}

int segmap_entry (const struct wlfs_super_meta *meta, __u32 segment) {
    return segment % segmap_entries(meta);
}

/*
 *  Checkpoint block helpers
 */

__u32 checkpoint_block (const struct wlfs_super_meta *meta, __u32 index) {
    return index / checkpoint_entries(meta);
}

__u32 checkpoint_blocks (const struct wlfs_super_meta *meta) {
    return MAX(ceil32(segmap_blocks(meta) + imap_blocks(meta),
                      checkpoint_entries(meta)),
               1);
}

int checkpoint_entries (const struct wlfs_super_meta *meta) {
    return block_bytes(meta) / sizeof(sector_t);
}

int checkpoint_entry (const struct wlfs_super_meta *meta, __u32 index) {
    return index % checkpoint_entries(meta);
}

__u32 checkpoint_index (const struct wlfs_super_meta *meta, __u32 index, 
                        unsigned type) {
    switch (type) {
    // If we have the index of an entry in the imap (i.e., an inode index)
    case IMAP_M:
        // Convert it to a block index
        index = imap_block(meta, index);
    // We have an imap block index
    case IMAP_M | BLOCK_M:;
        // Segmap blocks appear first in the checkpoint region
        __u32 tmp = segmap_blocks(meta);
#ifdef __KERNEL__
        BUG_ON(will_overflow(index + tmp, sizeof(index) << 3));
#endif
        index += tmp;
        break;

    // If we have the index of an entry in the segmap (i.e., a segment index)
    case SEGMAP_M:
        // Convert it to a block index
        index = segmap_block(meta, index);
    // Segmap block indices are already checkpoint indices; if the map bits
    // are empty, then we have a checkpoint index and this is a no-op
    default:
        break;
    }

    return index;
}

sector_t checkpoint_offset (const struct wlfs_super_meta *meta, __u32 index,
                            unsigned type) {
    sector_t start = checkpoint_start(meta);
    switch (type) {
    // We have a checkpoint index
    case ENTRY_M:
        // Convert it to a block index
        index = checkpoint_block(meta, index);
    // We have a checkpoint block
    case BLOCK_M:
        // Just increment the first checkpoint block number by the given index
        return start + index;

    // If a map bit is set
    default:
        // We need to convert the index to a checkpoint block index before
        // incrementing
        return start + checkpoint_block(meta, 
                                        checkpoint_index(meta, index, type));
    }
}
