#define _GNU_SOURCE
#include <argp.h>
#include <fcntl.h>
#include <linux/fs.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#include "block.h"
#include "util.h"
#include "wlfs.h"

// Data structure for holding parsed argp parameters
struct arguments {
    char *device;
    bool round;
    struct wlfs_super_meta sb;
};
// Return codes
enum return_code {
    SUCCESS,
    NO_MEMORY,          // Memory allocation failed
    DEVICE_ERROR,       // Something went wrong with the block device
    INVALID_ARGUMENT,   // An invalid argument was supplied
    ILLEGAL_CONFIG,     // Argument choice caused an illegal configuration
};

// Compute derived filesystem parameters (e.g., segment count) and either
// round or reject misaligned block/segment sizes
static enum return_code build_super (int fd, struct wlfs_super_meta *sb, 
                                     bool round);
// Calculate the number of segments in the block device; may return -1 if an
// error occurs
static __u32 segments (const struct wlfs_super_meta *sb, __u64 size);
// Argp argument parser
static error_t parse_opt (int key, char *arg, struct argp_state *state);
// Initialize the checkpoint region (write index numbers)
static enum return_code write_checkpoint (int fd, struct wlfs_super_meta *sb);
// Create and persist the imap
static enum return_code write_imap (int fd, struct wlfs_super_meta *sb);
// Create and persist the segmap
static enum return_code write_segmap (int fd, struct wlfs_super_meta *sb);
// Persist the superblock to the block device
static enum return_code write_super (int fd, struct wlfs_super_meta *sb);

// Description of argp keyword parameters
static struct argp_option options[] = {
    {"block-size", 'b', "size", 0, "Block size (bytes)"},
    {"buffer-period", 'w', "period", 0, "Write-back period (seconds)"},
    {"checkpoint-period", 'c', "period", 0, "Checkpoint period (seconds)"},
    {"indirection", 'i', "depth", 0, "Indirect block tree depth"},
    {"inodes", 'n', "num", 0, "Maximum number of inodes"},
    {"min-clean", 'm', "num", 0, 
     "Clean when the number of clean segments drops below this value"},
    {"segment-size", 's', "size", 0, "Segment size (bytes)"},
    {"target-clean", 't', "num", 0, 
     "Stop cleaning when the number of clean segments rises above this value"},
    {"round", 'r', 0, 0, 
     "Round block/segment size to the nearest sector/block boundary"},
    {0}
};
// Description of argp positional parameters
static char args_doc[] = "device";

int main (int argc, char **argv) {
    // Initialize argp parser
    struct argp argp = {options, parse_opt, args_doc};
    struct arguments arguments = {0};
    // Set default argument values
    arguments.sb.block_size = WLFS_BLOCK_SIZE;
    arguments.sb.buffer_period = BUFFER_PERIOD;
    arguments.sb.checkpoint_period = CHECKPOINT_PERIOD;
    arguments.sb.indirection = INDIRECTION;
    arguments.sb.magic = (__u32) WLFS_MAGIC;
    arguments.sb.inodes = MAX_INODES;
    arguments.sb.min_clean_segs = MIN_CLEAN_SEGS;
    arguments.sb.segment_size = SEGMENT_SIZE;
    arguments.sb.target_clean_segs = TARGET_CLEAN_SEGS;
    // Parse commandline arguments
    argp_parse(&argp, argc, argv, 0, 0, &arguments);

    int fd = open(arguments.device, O_RDWR);
    if (fd < 0) {
        fprintf(stderr, "Opening %s failed\n", arguments.device);
        return -DEVICE_ERROR;
    }

    // Sanitize input and compute derived values
    int ret = build_super(fd, &arguments.sb, arguments.round);
    if (ret != SUCCESS) {
        fprintf(stderr, "Failed to compute filesystem parameters for %s\n",
                arguments.device);
        goto exit;
    }
#ifndef NDEBUG
    printf("Block size: %hu\n"
           "Buffer period: %hhu\n"
           "Checkpoint blocks: %hu\n"
           "Checkpoint period: %hhu\n"
           "Indirection: %hhu\n"
           "Max inodes: %u\n"
           "Minimum clean segments: %hhu\n"
           "Segments: %u\n"
           "Segment size: %u\n"
           "Target clean segments: %hhu\n",
           arguments.sb.block_size, arguments.sb.buffer_period, 
           arguments.sb.checkpoint_blocks, arguments.sb.checkpoint_period,
           arguments.sb.indirection, arguments.sb.inodes,
           arguments.sb.min_clean_segs, arguments.sb.segments,
           arguments.sb.segment_size, arguments.sb.target_clean_segs);
#endif

    // Write super block to disk
    ret = write_super(fd, &arguments.sb);
    if (ret != SUCCESS) {
        fprintf(stderr, "Failed to write superblock to %s\n",
                arguments.device);
        goto exit;
    }
    // Initialize the ID's of all the checkpoint blocks
    ret = write_checkpoint(fd, &arguments.sb);
    if (ret != SUCCESS) {
        fprintf(stderr, "Failed to initialize checkpoint region of %s\n",
                arguments.device);
        goto exit;
    }
    // Write the first imap block, and update the checkpoint region
    ret = write_imap(fd, &arguments.sb);
    if (ret != SUCCESS) {
        fprintf(stderr, "Failed to write imap to %s\n", arguments.device);
        goto exit;
    }
    // Write the first segmap block, and update the checkpoint region
    ret = write_segmap(fd, &arguments.sb);
    if (ret != SUCCESS) {
        fprintf(stderr, "Failed to write segmap to %s\n", arguments.device);
    }

    fsync(fd);
exit:
    close(fd);
    return ret;
}

/*
 * Helper functions
 */

error_t parse_opt (int key, char *arg, struct argp_state *state) {
    struct arguments *arguments = (struct arguments *) state->input;
    __u64 value;
    if (arg) {
        value = atoi(arg);
    }

    switch (key) {
    case 'b':
        if (value < 1) {
            argp_error(state, "Block size of %lluB is too small\n", value);
        } else if (value > (__u64) getpagesize()){
            argp_error(state, "Block size must be <= %dB\n", getpagesize());
        }
        arguments->sb.block_size = value;
        break;

    case 'w':
        if (value < 1) {
            argp_error(state, 
                       "Write-back period of %llu seconds is too small\n", 
                       value);
        } else if (will_overflow(value, 8)) {
            argp_error(state, "Write-back period doesn't fit into 8 bits");
        }
        arguments->sb.buffer_period = value;
        break;

    case 'c':
        if (value < 1) {
            argp_error(state, 
                       "Checkpoint period of %llu seconds is too small\n", 
                       value);
        } else if (will_overflow(value, 8)) {
            argp_error(state, "Checkpoint period doesn't fit into 8 bits");
        }
        arguments->sb.checkpoint_period = value;
        break;

    case 'i':
        if (value == 0) {
            argp_error(state, "Indirection depth of %llu is too small\n", 
                       value);
        } else if (will_overflow(value, 8)) {
            argp_error(state, "Indirection doesn't fit into 8 bits");
        }
        arguments->sb.indirection = value;
        break;

    case 'n':
        if (value < 1) {
            argp_error(state, "%llu is not enough inodes\n", value);
        } else if (will_overflow(value, 32)) {
            argp_error(state, "Max inode count doesn't fit into 32 bits");
        }
        arguments->sb.inodes = value;
        break;

    case 'm':
        if (value < 1) {
            argp_error(state, 
                       "A threshold of %llu will never trigger cleaning\n", 
                       value);
        } else if (will_overflow(value, 8)) {
            argp_error(state, 
                       "Minimum clean segment count doesn't fit into 8 bits");
        }
        arguments->sb.min_clean_segs = value;
        break;

    case 's':
        if (value < WLFS_BLOCK_SIZE) {
            argp_error(state, "Segment size must be at least %dB\n",
                       WLFS_BLOCK_SIZE);
        } else if (will_overflow(value, 32)) {
            argp_error(state, "Segment size doesn't fit into 32 bits");
        }
        arguments->sb.segment_size = value;
        break;

    case 't':
        if (value < 1) {
            argp_error(state, "A threshold of %llu will prevent cleaning\n", 
                       value);
        } else if (will_overflow(value, 8)) {
            argp_error(state, "Target clean segments doesn't fit into 8 bits");
        }
        arguments->sb.buffer_period = value;
        break;

    case 'r':
        arguments->round = true;
        break;

    case ARGP_KEY_ARG:
        if (state->arg_num > 1) {
            argp_usage(state);
        }
        arguments->device = arg;
        break;

    case ARGP_KEY_END:
        if (state->arg_num < 1) {
            argp_usage(state);
        }
        break;

    default:
        return ARGP_ERR_UNKNOWN;
    }

    return 0;
}

enum return_code build_super (int fd, struct wlfs_super_meta *sb, bool round) {
    __u64 block_size;
    __u64 size;
    struct stat buf;

    // Find the physical block size & block device size; ioctl's will not work
    // when fd refers to a file, so fall back to fstat
    if (ioctl(fd, BLKPBSZGET, &block_size) < 0 || 
        ioctl(fd, BLKGETSIZE64, &size) < 0) {
        fprintf(stderr, "ioctl failed, falling back to fstat\n");
        if (fstat(fd, &buf) < 0) {
            fprintf(stderr, "fstat failed\n");
            return -DEVICE_ERROR;
        }
        block_size = buf.st_blksize;
        size = buf.st_size;
    }

    if (round) {
        // Round the block & segment sizes
        sb->block_size = (__u16) block_size;
        sb->segment_size += sb->segment_size % sb->block_size;
    // Reject incorrect sizes            
    } else if (sb->segment_size % sb->block_size != 0) {
        fprintf(stderr, 
                "%uB (segment size) %% %huB (block size) != 0, consider "
                "using -r\n", 
                sb->segment_size, 
                sb->block_size);
        return -INVALID_ARGUMENT;
    }
    
    // Set total number of segments
    __u32 nsegments = segments(sb, size);
    if (nsegments == 0) {
        fprintf(stderr, "Not enough space for wlfs\n");
        return -ILLEGAL_CONFIG;
    }
    sb->segments = nsegments;

    // Set the number of checkpoint blocks
    __u16 nblocks = checkpoint_blocks(sb);
    if (nblocks < 1) {
        fprintf(stderr, "Failed to get number of checkpoint blocks\n");
        return -ILLEGAL_CONFIG;
    }
    sb->checkpoint_blocks = nblocks;
    // Calculate the number of segments that are actually checkpoint blocks,
    // rounding up
    __u32 checkpoint_segments = 
        (sb->checkpoint_blocks * sb->block_size + sb->segment_size - 1) / 
        sb->segment_size;
    if (checkpoint_segments >= sb->segments) {
        fprintf(stderr, 
                "Checkpoint region requires %u segments, only %u available\n", 
                checkpoint_segments,
                sb->segments);
        return -ILLEGAL_CONFIG;
    }
    // Update segments to account for checkpoint blocks
    sb->segments -= checkpoint_segments;

    return SUCCESS;
}

__u32 segments (const struct wlfs_super_meta *sb, __u64 size) {
    __u64 segments = (size - WLFS_OFFSET - sb->block_size) / sb->segment_size;
    if (will_overflow(segments, 32)) {
        fprintf(stderr, "Number of segments doesn't fit into 32 bits\n");
        return -INVALID_ARGUMENT;
    }
    return segments;
}

enum return_code write_super (int fd, struct wlfs_super_meta *sb) {
    // Write super block to the correct sector of the block device
    ssize_t ret = pwrite(fd, sb, sizeof(struct wlfs_super_meta), WLFS_OFFSET);
    if (ret < 0) {
        fprintf(stderr, "superblock write failed with error code %zd\n", ret);
        return DEVICE_ERROR;
    }

    return SUCCESS;
}

enum return_code write_checkpoint (int fd, struct wlfs_super_meta *sb) {
    enum return_code ret = NO_MEMORY;
    struct block *buffer = (struct block *) calloc (1, sizeof(struct block));
    if (!buffer) {
        fprintf(stderr, "Failed to allocate checkpoint block buffer\n");
        return ret;
    }
    // Write all the checkpoint block headers to the device
    ssize_t write_ret;
    for (int i = 0; i < sb->checkpoint_blocks; ++i) {
        // Populate buffer fields
        buffer->index = i;
        buffer->h0.wtime = (__kernel_time_t) time(NULL);
        // Write buffer to the head of a checkpoint block
        write_ret = pwrite(fd, buffer, sizeof(struct block), 
                           (checkpoint_start(sb) + i) * sb->block_size);
        if (write_ret < 0) {
            fprintf(stderr, "Failed to write checkpoint block %i\n", i);
            ret = DEVICE_ERROR;
            goto exit;
        }
    }
    ret = SUCCESS;

exit:
    free(buffer);
    return ret;
}

enum return_code write_imap (int fd, struct wlfs_super_meta *sb) {
    struct block *check_block = (struct block *) calloc(1, sb->block_size);
    if (!check_block) {
        fprintf(stderr, "Failed to allocate checkpoint block buffer\n");
        return NO_MEMORY;
    }
    struct block *imap_block = (struct block *) calloc(
        1, sizeof(struct block));
    if (!imap_block) {
        fprintf(stderr, "Failed to allocate imap block buffer\n");
        free(check_block);
        return NO_MEMORY;
    }

    int index = checkpoint_index(sb, ROOT_INODE_INDEX, IMAP_M | BLOCK_M);
    off_t offset = 
        checkpoint_offset(sb, checkpoint_block(sb, index), BLOCK_M) * 
        sb->block_size;
    // Read the checkpoint block from the device
    ssize_t ret = pread(fd, check_block, sb->block_size, offset);
    if (ret < 0) {
        fprintf(stderr, "imap checkpoint block read failed: %zd\n", ret);
        goto exit;
    }
    // Store the location of the imap block in the checkpoint block
    sector_t *check_entry = block_entry(
        check_block, checkpoint_entry(sb, index), sector_t);
    // The first imap block will be written to first segment
    *check_entry = segment_start(sb, 0);

    // Write both blocks to the device
    imap_block->h0.wtime = check_block->h0.wtime =
        (__kernel_time_t) time(NULL);
    ret = pwrite(fd, check_block, sb->block_size, offset);
    if (ret < 0) {
        fprintf(stderr, "imap checkpoint block write failed: %zd\n", ret);
        goto exit;
    }
    ret = pwrite(fd, imap_block, sizeof(struct block), 
                 *check_entry * sb->block_size);
    if (ret < 0) {
        fprintf(stderr, "imap block write failed: %zd\n", ret);
    }

exit:
    free(check_block);
    free(imap_block);

    return ret >= 0 ? SUCCESS : DEVICE_ERROR;
}

enum return_code write_segmap (int fd, struct wlfs_super_meta *sb) {
    struct block *check_block = (struct block *) calloc(1, sb->block_size);
    if (!check_block) {
        fprintf(stderr, "Failed to allocate checkpoint block buffer\n");
        return NO_MEMORY;
    }
    struct block *segmap_block = (struct block *) calloc(1, sb->block_size);
    if (!segmap_block) {
        fprintf(stderr, "Failed to allocate segmap block buffer\n");
        free(check_block);
        return NO_MEMORY;
    }

    off_t offset = checkpoint_block(sb, 0);
    // Read the checkpoint block from the device
    ssize_t ret = pread(fd, check_block, sb->block_size, offset);
    // Store the location of the segmap block in the checkpoint block
    sector_t *check_entry = (sector_t *) (check_block + 1);
    // The first segmap block will be written to first segment, past the imap
    // block
    *check_entry = segment_start(sb, 0) + 1;

    // Set the value of the segmap entry (there will be two blocks: one
    // segmap and one imap block)
    __segmap_t *segmap_entry = (__segmap_t *) (segmap_block + 1);
    *segmap_entry = 2;

    // Write both blocks to the device
    segmap_block->h0.wtime = check_block->h0.wtime = 
        (__kernel_time_t) time(NULL);
    ret = pwrite(fd, check_block, sb->block_size, offset);
    if (ret < 0) {
        fprintf(stderr, "segmap checkpoint block write failed: %zd\n", ret);
        goto exit;
    }
    ret = pwrite(fd, segmap_block, sb->block_size,
                 *check_entry * sb->block_size);
    if (ret < 0) {
        fprintf(stderr, "segmap block write failed: %zd\n", ret);
    }

exit:
    free(check_block);
    free(segmap_block);

    return ret >= 0 ? SUCCESS : DEVICE_ERROR;
}
