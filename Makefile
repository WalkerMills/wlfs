# WLFS kernel module Makefile

include $(PWD)/config.mk

SRC := block.c init.c cache.c super.c util.c
TESTS := unit_test validation_test

obj-m := wlfs.o
wlfs-objs := $(SRC:%.c=$(SRC_DIR)/%.o) $(TEST_DIR)/kern_test.o 
ccflags-y := -I$(INCLUDE_DIR) -Wno-declaration-after-statement $(OPT)

.PHONY: all clean test

all: ko mkfs-wlfs

clean:
	$(MAKE) -C $(KDIR)/build M=$(PWD) clean
	$(RM) mkfs-wlfs *.o $(TEST_DIR)/*.o $(foreach t,$(TESTS),$(TEST_DIR)/$(t))

test: all $(M4_CONFIG) $(TESTS)
ifndef OPT
	$(M4) test.sh | bash
endif

ko:
	$(MAKE) -C $(KDIR)/build M=$(PWD) modules

mkfs-wlfs.o: private CFLAGS = -I$(INCLUDE_DIR) -std=c11 $(OPT) 
CXXFLAGS := -I$(INCLUDE_DIR) $(OPT) 

%_user.o: $(SRC_DIR)/%.c
	$(CC) $(CFLAGS) -I$(INCLUDE_DIR) $(OPT) -c -o $@ $<
mkfs-wlfs: block_user.o util_user.o

GTEST_CFLAGS := -DGTEST_HAS_PTHREAD=0
GTEST_LDFLAGS := -lgtest
NL_CFLAGS := -I/usr/include/libnl3
NL_LDFLAGS := -lnl-3

$(TEST_DIR)/unit_test.o: private CXXFLAGS += $(NL_CFLAGS) -Wno-write-strings
unit_test: private LDFLAGS += $(NL_LDFLAGS)
unit_test: block_user.o util_user.o
validation_test: block_user.o util_user.o

$(TEST_DIR)/%.o: $(TEST_DIR)/%.cpp $(M4_CONFIG)
	$(M4) $< | \
	$(CXX) $(CXXFLAGS) $(GTEST_CFLAGS) -x c++ -std=c++11 -Wall -c -o $@ - 

%_test: $(TEST_DIR)/%_test.o
	$(CXX) $(LDFLAGS) $(GTEST_LDFLAGS) -o $(TEST_DIR)/$@ $^
