#include <fcntl.h>
#include <gtest/gtest.h>
#include <linux/loop.h>
#include <linux/types.h>
#include <netlink/netlink.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mount.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <unistd.h>

#include "block.h"
#include "shared_test.h"
#include "util.h"

// Return a new, connected netlink socket
static struct nl_sock *new_nl_socket () {
    struct nl_sock *socket = nl_socket_alloc();
    if (!socket) {
        fprintf(stderr, "Failed to create netlink socket from userspace\n");
        return NULL;
    }
    int ret = nl_connect(socket, NETLINK_USERSOCK);
    if (ret < 0) {
        nl_perror(ret, "nl_connect");
        return NULL;
    }

    return socket;
}

// Close and free a connected netlink socket
static void free_nl_socket (struct nl_sock *socket) {
    nl_close(socket);
    nl_socket_free(socket);
}

// Use a netlink socket to call a kernelspace unit test function
static int call_kernel_test (struct nl_sock *socket, enum test_func func, 
                             void *args) {
    char msg[BUF_SIZE];
    sprintf(msg, "%u%s%llx", func, SEP, (__u64) args);
    // Call the kernel-side unit test
    int ret = nl_send_simple(socket, MSG_TYPE, 0, msg, BUF_SIZE);
    if (ret < 0) {
        nl_perror(ret, "nl_send_simple");
    }

    return ret;
}

// Receive a message from the kernel side of a netlink socket
static struct nlmsghdr *receive_from_kernel (struct nl_sock *socket, int len) {
    struct sockaddr_nl src_addr = {0};
    struct nlmsghdr *nl_header =
        (struct nlmsghdr *) calloc(1, NLMSG_SPACE(len));
    nl_header->nlmsg_len = NLMSG_SPACE(len);

    struct iovec iov;
    iov.iov_base = nl_header;
    iov.iov_len = nl_header->nlmsg_len;

    struct msghdr header;
    header.msg_name = &src_addr;
    header.msg_namelen = sizeof(struct sockaddr_nl);
    header.msg_iov = &iov;
    header.msg_iovlen = 1;

    int read = recvmsg(nl_socket_get_fd(socket), &header, 0);
    if (read < 0) {
        free(nl_header);
        return NULL;
    }

    return nl_header;
}

#ifndef NDEBUG
/*
 * Util unit test(s)
 */

TEST (test_util, test_max_bytes) {
    struct nl_sock *socket = new_nl_socket();
    ASSERT_TRUE(socket != NULL);
    struct test_max_bytes_args args = {0};
    args.meta.block_size = m4_BLOCK_SIZE;
    args.meta.indirection = 0;

    // Call the kernel-side unit test
    ASSERT_GE(call_kernel_test(socket, TEST_MAX_BYTES, (void *) &args), 0);
    // Get its return (max bytes per file)
    struct nlmsghdr *nl_header = receive_from_kernel(socket, BUF_SIZE);
    ASSERT_TRUE(nl_header != NULL);
    // For indirection depth of zero, the total number of block pointers is
    // fixed
    __u64 entries = NBLOCK_PTR;
    ASSERT_EQ(entries * args.meta.block_size, 
              strtoull((char *) NLMSG_DATA(nl_header), NULL, 10));
    free(nl_header);

    // Update the arguments and call the kernel-side unit test again
    args.meta.indirection = 1;
    ASSERT_GE(call_kernel_test(socket, TEST_MAX_BYTES, (void *) &args), 0);
    nl_header = receive_from_kernel(socket, BUF_SIZE);
    ASSERT_TRUE(nl_header != NULL);
    // Increase the total number of block pointers by the number in an 
    // indirect block, minus the indirect block pointer
    __u64 block_entries = block_bytes(&args.meta) / sizeof(__u64); 
    entries = entries - 1 + block_entries;
    ASSERT_EQ(entries * args.meta.block_size, 
              strtoull((char *) NLMSG_DATA(nl_header), NULL, 10));
    free(nl_header);

    args.meta.indirection = 2;
    ASSERT_GE(call_kernel_test(socket, TEST_MAX_BYTES, (void *) &args), 0);
    nl_header = receive_from_kernel(socket, BUF_SIZE);
    ASSERT_TRUE(nl_header != NULL);
    // Add the double indirect block pointers
    entries = entries - 1 + block_entries * block_entries;
    ASSERT_EQ(entries * args.meta.block_size, 
              strtoull((char *) NLMSG_DATA(nl_header), NULL, 10));
    free(nl_header);

    // Clean up the netlink socket
    free_nl_socket(socket);
}

/*
 * Segment map unit tests
 */

enum mount_return_code {
    FAILURE = -1,   // (Un)mounting failed
    SUCCESS,        // (Un)mounting block/character device successful
    LOOP,           // (Un)mounting loop device successful
};

struct mount_ret {
    enum mount_return_code code;
    int loop;   // If code == LOOP, this is the loop device number
};

#define LOOP_PREFIX "/dev/loop" // Prefix for loop devices
#define LOOP_PATH_LEN 16        // Maximum path length of a loop device

static struct mount_ret mount_device (char *dev, char *target) {
    struct mount_ret ret;
    ret.code = FAILURE;
    // Open the device to mount
    int dev_fd = open(dev, O_RDWR);
    if (dev_fd < 0) {
        fprintf(stderr, "Failed to open %s\n", dev);
        return ret;
    }

    struct stat buf;
    if (fstat(dev_fd, &buf) < 0) {
        fprintf(stderr, "Failed to stat %s\n", dev);
        goto exit;
    }
    switch (buf.st_mode & S_IFMT) {
    // If the device is a file, we have to use a loop interface to mount it
    case S_IFREG: {
        // Find a free loop device
        int loopctl_fd = open(LOOP_PREFIX "-control", O_RDWR);
        if (loopctl_fd < 0) {
            fprintf(stderr, "Failed to open " LOOP_PREFIX "-control\n");
            goto exit;
        }
        int loop_no = ioctl(loopctl_fd, LOOP_CTL_GET_FREE);
        close(loopctl_fd);
        if (loop_no < 0) {
            fprintf(stderr, "No free loop devices\n");
            goto exit;
        }

        // Attach the file to the loop device
        char loop_dev[LOOP_PATH_LEN];
        sprintf(loop_dev, LOOP_PREFIX "%i", loop_no);
        int loopdev_fd = open(loop_dev, O_RDWR);
        if (loopdev_fd < 0) {
            fprintf(stderr, "Failed to open loop device %s\n", loop_dev);
            goto exit;
        }
        if (ioctl(loopdev_fd, LOOP_SET_FD, dev_fd) < 0) {
            fprintf(stderr, "Failed to set loop device file descriptor\n");
            close(loopdev_fd);
            goto exit;
        }
        close(loopdev_fd);

        // Mount the loop device
        if (mount(loop_dev, target, WLFS_NAME, 0, NULL) < 0) {
            fprintf(stderr, "Mounting %s on %s via %s failed\n", dev, target, 
                    loop_dev);
            goto exit;
        }
        ret.code = LOOP;
        ret.loop = loop_no;
        break;
    }

    // If the device is a character or block device
    case S_IFCHR:
    case S_IFBLK:
        if (mount(dev, target, WLFS_NAME, 0, NULL) < 0) {
            fprintf(stderr, "Mounting %s on %s failed\n", dev, target);
            goto exit;
        }
        ret.code = SUCCESS;
        break;

    default:
        fprintf(stderr, "Unsupported device type\n");
        goto exit;
    }

exit:
    close(dev_fd);
    return ret;
}

static enum mount_return_code umount_device (char *target, 
                                             struct mount_ret mount_ret) {
    enum mount_return_code ret = FAILURE;
    // Unmount the device
    if (umount(target) < 0) {
        fprintf(stderr, "Failed to unmount %s\n", target);
        return ret;
    }

    // If this is a loop device, reset the file descriptor of the loop device
    if (mount_ret.code == LOOP) {
        // Open the loop device
        char loop_dev[LOOP_PATH_LEN];
        sprintf(loop_dev, LOOP_PREFIX "%i", mount_ret.loop);
        int loop_fd = open(loop_dev, O_RDWR);
        if (loop_fd < 0) {
            fprintf(stderr, "Failed to open loop device %s\n", loop_dev);
            return ret;
        }
        // Clear the file descriptor
        if (ioctl(loop_fd, LOOP_CLR_FD, 0) < 0) {
            fprintf(stderr, "Failed to clear file descriptor of %s\n", 
                    loop_dev);
            close(loop_fd);
            return ret;
        }
        close(loop_fd);
        ret = LOOP;
    } else {
        ret = SUCCESS;
    }

    return ret;
}

class test_block_cache : public ::testing::Test {
protected:
    int fd;
    struct wlfs_super_meta meta = {0};
    struct mount_ret mount_ret;

    virtual void SetUp () {
        // Open the block device
        fd = open("m4_DEVICE", O_RDWR);
        ASSERT_GE(fd, 0);
        // Read the superblock
        ASSERT_GT(
            pread(fd, &meta, sizeof(struct wlfs_super_meta), WLFS_OFFSET), 
            0);

        // Mount the filesystem (segmap usage requires an initialized
        // superblock in memory)
        mount_ret = mount_device("m4_DEVICE", "m4_MOUNT");
        ASSERT_NE(mount_ret.code, FAILURE);
    }

    virtual void TearDown () {
        // Unmount the filesystem
        enum mount_return_code umount_ret = umount_device("m4_MOUNT",
                                                          mount_ret);
        ASSERT_NE(umount_ret, FAILURE);
    }
};

TEST_F (test_block_cache, test_checkpoint_block_cache) {
    // Initialize test parameters
    struct test_checkpoint_block_cache_args args = {
        .block_size = meta.block_size,
        .entry_size = sizeof(__u64),
        .index = {
            0,
            checkpoint_block(&meta, 
                             checkpoint_index(&meta, meta.inodes, IMAP_M)),
        },
        .offset = {
            checkpoint_offset(&meta, 0, BLOCK_M),
            checkpoint_offset(&meta, meta.inodes, IMAP_M),
        },
        .entry_index = checkpoint_index(&meta, meta.inodes, IMAP_M),
        .entry = 0xDE7EC7ED,
    };

    // Call kernel space test
    struct nl_sock *socket = new_nl_socket();
    ASSERT_TRUE(socket != NULL);
    ASSERT_GE(
        call_kernel_test(socket, TEST_CHECKPOINT_BLOCK_CACHE, (void *) &args), 
        0);
    // Get the test's return value
    struct nlmsghdr *header = receive_from_kernel(socket, BUF_SIZE);
    free_nl_socket(socket);
    ASSERT_TRUE(header != NULL);
    long long ret = strtoll((char *) NLMSG_DATA(header), NULL, 10);
    free(header);
    // Check the return value
    ASSERT_GE(ret, 0);
}
#endif

int main (int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
