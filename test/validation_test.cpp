#define _XOPEN_SOURCE 200809L // pread

#include <errno.h>
#include <fcntl.h>
#include <gtest/gtest.h>
#include <unistd.h>

#include "block.h"
#include "util.h"
#include "wlfs.h"

class test_mkfs_wlfs : public ::testing::Test {
protected:
    // File descriptor of test block device
    int fd;
    // Wlfs superblock metadata
    struct wlfs_super_meta meta = {0};

    virtual void SetUp () {
        fd = open("m4_DEVICE", O_RDWR);
        ASSERT_GE(fd, 0);
        ASSERT_GE(
            pread(fd, &meta, sizeof(struct wlfs_super_meta), WLFS_OFFSET),
            0);
    }

    virtual void TearDown () {
        close(fd);
    }
};

TEST_F (test_mkfs_wlfs, test_write_super) {
    ASSERT_EQ(meta.magic, WLFS_MAGIC);
}

TEST_F (test_mkfs_wlfs, test_write_checkpoint) {
    struct block *buffer = (struct block *) calloc(1, meta.block_size);
    off_t offset = WLFS_OFFSET + meta.block_size;
    for (__u64 i = 0; i < meta.checkpoint_blocks; ++i) {
        ASSERT_GT(pread(fd, buffer, meta.block_size, offset), 0);
        ASSERT_EQ(buffer->index, i);
        offset += meta.block_size;
    }
    free(buffer);
}

TEST_F (test_mkfs_wlfs, test_write_imap) {
    // first imap block
    struct block *block = (struct block *) calloc(1, meta.block_size);
    ASSERT_GT(
        pread(
            fd, 
            block, 
            meta.block_size, 
            meta.block_size * checkpoint_block(
                &meta, 
                checkpoint_index(&meta, ROOT_INODE_INDEX, IMAP_M))),
        0);
    // Get the segmap block's index
    sector_t checkpoint_entry = *block_entry(block, 0, sector_t);
    ASSERT_NE(checkpoint_entry, 0);
    memset(block, 0, sizeof(struct block) + sizeof(sector_t));

    // Read the imap block
    ASSERT_GT(
        pread(fd, block, meta.block_size,
              WLFS_OFFSET + meta.block_size * (meta.checkpoint_blocks + 1)),
        0);
    // There should be no inodes
    sector_t imap_entry = *block_entry(block, 0, sector_t);
    ASSERT_EQ(imap_entry, 0);
    // But the write time should be nonzero
    ASSERT_GT(block->h0.wtime, 0);
    free(block);
}

TEST_F (test_mkfs_wlfs, test_write_segmap) {
    ASSERT_EQ(meta.current_segment, 0);
    // Read the first checkpoint block to get the block number of the first
    // segmap block
    struct block *block = (struct block *) calloc(1, meta.block_size);
    ASSERT_GT(
        pread(fd, block, meta.block_size, 
              meta.block_size * checkpoint_block(&meta, 0)), 
        0);
    // Get the segmap block's index
    sector_t checkpoint_entry = *block_entry(block, 0, sector_t);
    ASSERT_NE(checkpoint_entry, 0);
    memset(block, 0, sizeof(struct block) + sizeof(sector_t));

    // Read the segmap block
    ASSERT_GT(
        pread(fd, block, meta.block_size,
              WLFS_OFFSET + meta.block_size * (meta.checkpoint_blocks + 2)),
        0);
    // There should be two blocks used (by the imap & segmap blocks)
    __segmap_t segmap_entry = *block_entry(block, 0, __segmap_t);
    free(block);
    ASSERT_EQ(segmap_entry, 2);
}

int main (int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
