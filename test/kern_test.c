#ifndef NDEBUG
#include <linux/blkdev.h>
#include <linux/buffer_head.h>
#include <linux/fs.h>
#include <linux/kernel.h>
#include <linux/list.h>
#include <linux/module.h>
#include <linux/printk.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <net/sock.h>
#include <net/netlink.h>

#include "cache.h"
#include "kern_test.h"
#include "shared_test.h"
#include "util.h"
#include "wlfs.h"

// Send a message to user space through a netlink socket
static int send_to_user (struct sk_buff *skb_in, char *buf, int len) {
    struct sk_buff *skb_out = nlmsg_new(len, GFP_NOFS);
    if (!skb_out) {
        return -ENOMEM;
    }
    struct nlmsghdr *header = nlmsg_put(skb_out, 0, 0, MSG_TYPE, len, 0);
    memcpy(NLMSG_DATA(header), buf, len);
    return nlmsg_unicast(skb_in->sk, skb_out, NETLINK_CB(skb_in).portid);
}

/*
 * Util unit test (kernel space helpers)
 */

int test_max_bytes (struct sk_buff *skb, void __user *src) {
    ENTER_FN

    __u64 bytes = 0;
    struct test_max_bytes_args *args = (struct test_max_bytes_args *) 
        kzalloc(sizeof(struct test_max_bytes_args), GFP_NOFS);
    if (!args) {
        printk(KERN_ERR "Failed to allocate %s_args\n", __FUNCTION__);
        goto exit;
    }
    // Copy args into kernel space
    if (copy_from_user(args, src, sizeof(struct test_max_bytes_args)) > 0) {
        printk(KERN_ERR "Failed to copy %s_args from user space\n",
               __FUNCTION__);
        kfree(args);
        goto exit;
    }

    bytes = max_bytes(&args->meta);
    kfree(args);

exit:;
    char buf[BUF_SIZE] = {0};
    snprintf(buf, BUF_SIZE, "%llu", bytes);

    return send_to_user(skb, buf, strlen(buf) + 1);
}

/*
 *  Kernel space block cache unit test(s)
 */

// Get a reference to a live wlfs superblock
static struct super_block *get_sb (void);

// Helper for persisting checkpoint blocks
void persist_checkpoint_block (struct super_block *sb, struct block *block);

int test_checkpoint_block_cache (struct sk_buff *skb, void __user *src) {
    ENTER_FN

    long long ret = -ENOMEM;
    struct test_checkpoint_block_cache_args *args = 
        (struct test_checkpoint_block_cache_args *) kzalloc(
            sizeof(struct test_checkpoint_block_cache_args), GFP_NOFS);
    if (!args) {
        printk(KERN_ERR "Failed to allocate %s_args\n", __FUNCTION__);
        goto exit;
    }
    // Copy args into kernel space
    if (copy_from_user(args, src, 
                       sizeof(struct test_checkpoint_block_cache_args)) > 0) {
        printk(KERN_ERR "Failed to copy %s_args from user space\n",
               __FUNCTION__);
        kfree(args);
        goto exit;
    }

    struct super_block *sb = get_sb();
    struct block_cache *cache = alloc_block_cache(
        sb, "testmap", __TEST_CHECKPOINT_BLOCK_CACHE_BLOCKS - 1, 
        args->block_size, args->entry_size, GFP_NOFS, NULL);

    // Test block caching
    struct block *block = cache_block(cache, args->offset[0]);
    if (IS_ERR(block)) {
        printk(KERN_ERR "%s:%u Caching block %llu from %llu failed: %li\n", 
               __FUNCTION__, __LINE__, args->index[0], args->offset[0],
               PTR_ERR(block));
        ret = PTR_ERR(block);
        goto exit_free;
    }
    if (args->index[0] != block->index) {
        printk(KERN_ERR 
               "%s:%u Block index (from %llu) does not match (%llu != %llu)", 
               __FUNCTION__, __LINE__, args->offset[0], args->index[0],
               block->index);
        ret = -EINVAL;
        goto exit_free;
    }

    // Test accessing cached block
    struct block *tmp = access_block(cache, args->index[0], BLOCK_M);
    if (tmp != block) {
        printk(KERN_ERR "%s:%u Accessing cached block failed (%lX != %lX)\n", 
               __FUNCTION__, __LINE__, (uintptr_t) tmp, (uintptr_t) block);
        ret = -EILSEQ;
        goto exit_free;
    }

    // Decrement ref counts (active blocks cannot be evicted)
    release_block(cache, block);
    release_block(cache, tmp);

    // Test eviction of inactive blocks to cache new ones
    block = get_block(cache, args->offset[1], args->index[1], BLOCK_M);
    if (IS_ERR(block)) {
        printk(KERN_ERR "%s:%u Caching block %llu from %llu failed: %li\n",
               __FUNCTION__, __LINE__, args->index[1], args->offset[1],
               PTR_ERR(block));
        ret = PTR_ERR(block);
        goto exit_free;
    }
    if (args->index[1] != block->index) {
        printk(
            KERN_ERR 
            "%s:%u Block index (from %llu) does not match (%llu != %llu)\n", 
            __FUNCTION__, __LINE__, args->offset[1], args->index[1],
            block->index);
        ret = -EINVAL;
        goto exit_free;
    }

    // Check that active blocks will not be evicted
    shrink_block_cache(cache);
    tmp = access_block(cache, args->index[1], BLOCK_M);
    if (tmp == NULL) {
        printk(KERN_ERR "%s:%u Block %llu was freed while active\n",
               __FUNCTION__, __LINE__, args->index[1]);
        ret = -ENODATA;
        goto exit_free;
    }
    release_block(cache, block);
    release_block(cache, tmp);

    // The following tests are only valid if the two blocks are not the same
    if (args->index[0] == args->index[1]) {
        ret = 0;
        goto exit_free;
    }

    // Test accessing evicted blocks
    block = access_block(cache, args->index[0], BLOCK_M);
    if (block != NULL) {
        printk(KERN_ERR 
               "%s:%u Accessing uncached block %llu did not fail\n", 
               __FUNCTION__, __LINE__, args->index[0]);
        ret = -EFAULT;
        goto exit_free;
    }

    // Check that shrinking frees inactive blocks
    shrink_block_cache(cache);
    block = access_block(cache, args->index[1], BLOCK_M);
    if (block != NULL) {
        printk(KERN_ERR 
               "%s:%u Accessing uncached block %llu did not fail\n", 
               __FUNCTION__, __LINE__, args->index[1]);
        ret = -EFAULT;
        goto exit_free;
    }

    // Check that the cache can handle more than one block
    set_limit(cache, __TEST_CHECKPOINT_BLOCK_CACHE_BLOCKS);
    block = get_block(cache, args->offset[0], args->index[0], BLOCK_M);
    tmp = get_block(cache, args->offset[1], args->index[1], BLOCK_M);
    if (tmp == NULL || block == NULL || tmp == block || 
        tmp->index == block->index) {
        printk(KERN_ERR "%s:%u Increasing cache size failed\n", 
               __FUNCTION__, __LINE__);
        ret = -ENODATA;
        goto exit_free;
    }

    // Check that trying to set entries in the wrong block fails
    int success = 
        set_entry_cached(cache, block, args->entry_index, &args->entry);
    if (success != -EINVAL) {
        printk(
            KERN_ERR "%s:%u set_entry_cached failed to handle bad argument\n",
            __FUNCTION__, __LINE__);
        ret = -EILSEQ;
        goto exit_free;
    }
    // Check that we can set & get entries in the appropriate block
    success = set_entry_cached(cache, tmp, args->entry_index, &args->entry);
    if (success != 0) {
        printk(KERN_ERR "%s:%u set_entry_cached failed to run\n",
               __FUNCTION__, __LINE__);
        ret = -EILSEQ;
        goto exit_free;
    }
    __u64 dest = 0;
    success = get_entry_cached(cache, tmp, args->entry_index, &dest);
    if (success != 0) {
        printk(KERN_ERR "%s:%u get_entry_cached failed to run\n",
               __FUNCTION__, __LINE__);
        ret = -EILSEQ;
        goto exit_free;
    }
    if (dest != args->entry) {
        printk(KERN_ERR "%s:%u set_entry_cached failed to store data\n",
               __FUNCTION__, __LINE__);
        ret = -EILSEQ;
        goto exit_free;
    }

    // Test movement between inactive/active queues; since all blocks were
    // just added and still active, the inactive queue should be empty
    if (cache->active == NULL || cache->inactive != NULL) {
        printk(KERN_ERR "%s:%u Queue management error in __push_new_block\n",
               __FUNCTION__, __LINE__);
        ret = -EILSEQ;
        goto exit_free;
    }
    // Check that blocks whose ref count drops to zero are put into the
    // inactive queue
    release_block(cache, block);
    if (cache->active == NULL || cache->inactive == NULL) {
        printk(
            KERN_ERR 
            "%s:%u Queue management error in __decrement_block_refs\n",
            __FUNCTION__, __LINE__);
        ret = -EILSEQ;
        goto exit_free;
    }
    // Check that inactive blocks are put into the active queue when accessed
    // (i.e., ref count rises from 0)
    block = access_block(cache, args->index[0], BLOCK_M);
    if (cache->active == NULL || cache->inactive != NULL) {
        printk(KERN_ERR
               "%s:%u Queue management error in __increment_block_refs\n",
               __FUNCTION__, __LINE__);
        ret = -EILSEQ;
        goto exit_free;
    }

    // Test block persistence
    cache->persist = &persist_checkpoint_block;
    // Trigger a call to the persistence handler
    release_block(cache, tmp);
    shrink_block_cache(cache);
    // Check that the entry modified early was written to the device
    dest = 0;
    success = get_entry(cache, args->offset[1], args->entry_index, &dest);
    if (success != 0) {
        printk(KERN_ERR "%s:%u get_entry failed to run\n", __FUNCTION__,
               __LINE__);
        ret = -EILSEQ;
        goto exit_free;
    }
    if (dest != args->entry) {
        printk(KERN_ERR
               "%s:%u Failed to persist block; expected %llu, got %llu\n",
               __FUNCTION__, __LINE__, args->entry, dest);
        ret = -ENODATA;
        goto exit;
    }

    ret = 0;

exit_free:
    // Test that freeing the cache frees both inactive & active blocks
    free_block_cache(cache);
exit:;
    char buf[BUF_SIZE] = {0};
    snprintf(buf, BUF_SIZE, "%lli", ret);

    return send_to_user(skb, buf, strlen(buf) + 1);
}

struct super_block *get_sb (void) {
    // Get the filesystem type for this module
    struct file_system_type *wlfs_type = get_fs_type(WLFS_NAME);
    if (IS_ERR(wlfs_type)) {
        printk(KERN_ERR "Failed to get filesystem type with code %li\n",
               PTR_ERR(wlfs_type));
        return (struct super_block *) wlfs_type;
    }
    // Decrement the ref count of the module incremented by get_fs_type; this
    // simplifies cleanup, but if the ref count drops to zero, it will
    // invalidate the superblock reference.  Since this code is loaded into
    // the wlfs module, this should not be possible
    module_put(wlfs_type->owner);
    // Check if there's a wlfs superblock in memory
    if (wlfs_type->fs_supers.first == NULL) {
        printk(KERN_ERR "No filesystem instances found\n");
        return ERR_PTR(-ENODATA);
    }
    // Get the superblock for this filesystem
    return hlist_entry(wlfs_type->fs_supers.first, struct super_block, 
                       s_instances);
}

void persist_checkpoint_block (struct super_block *sb, struct block *block) {
    ENTER_FN

    struct wlfs_super_meta *meta = 
        &((struct wlfs_super *) sb->s_fs_info)->meta;
    // Device block number of this checkpoint block
    sector_t offset = checkpoint_offset(meta, block->index, BLOCK_M);
    // Get a buffer for the device block
    struct buffer_head *bh = sb_getblk(sb, offset);
    // Update its data
    memcpy(bh->b_data, block, meta->block_size);
    // Write it to the device
    mark_buffer_dirty(bh);
    sync_dirty_buffer(bh);
    brelse(bh);
}

#endif
